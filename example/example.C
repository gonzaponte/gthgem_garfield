#include <cstdlib>
#include <iostream>
#include <fstream>

#include <TApplication.h>
#include <TCanvas.h>
#include <TH1F.h>

#include "Garfield/ComponentComsol.hh"
#include "Garfield/ViewField.hh"
#include "Garfield/ViewFEMesh.hh"
#include "Garfield/MediumMagboltz.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/AvalancheMicroscopic.hh"
#include "Garfield/AvalancheMC.hh"
#include "Garfield/Random.hh"

using namespace Garfield;

int main(int argc, char * argv[]) {

  TApplication app("app", &argc, argv);

  // Load the field map.
  ComponentComsol fm;
  fm.Initialise("mesh.mphtxt", "dielectrics.dat", "field.txt", "mm");
  fm.EnableMirrorPeriodicityX();
  fm.EnableMirrorPeriodicityY();
  fm.PrintRange();

  // Dimensions of the THGEM [cm]
  constexpr double pitch = 0.07;

  ViewField fieldView;
  constexpr bool plotField = false;
  if (plotField) {
    fieldView.SetComponent(&fm);
    // Set the normal vector of the viewing plane (xz plane).
    fieldView.SetPlane(0, -1, 0, 0, 0, 0);
    // Set the plot limits in the current viewing plane.
    fieldView.SetArea(-pitch, -0.4, pitch, 0.4);
    fieldView.SetVoltageRange(-1600., 1600.);
    TCanvas* cf = new TCanvas("cf", "", 600, 600);
    cf->SetLeftMargin(0.16);
    fieldView.SetCanvas(cf);
    fieldView.PlotContour();
  }

  // Setup the gas.
  MediumMagboltz gas;
  gas.SetComposition("xe", 100.);
  gas.SetTemperature(180.);
  gas.SetPressure(1.6 * 750);
  gas.LoadGasFile("/home/gonzalo/data/xe_180K_1.6bar_1e+00_1e+05_2.gas");
  gas.Initialise(true);

  // Associate the gas with the corresponding field map material.
  fm.SetGas(&gas);
  fm.PrintMaterials();

  // Create the sensor.
  Sensor sensor;
  sensor.AddComponent(&fm);
  sensor.SetArea(-pitch, -pitch, -4e-1,
                  pitch,  pitch,  4e-1);

//  AvalancheMicroscopic aval;
  AvalancheMC aval;
  aval.SetSensor(&sensor);
  aval.SetDistanceSteps(3.e-3);
  aval.SetTimeSteps(3.e-2);

  AvalancheMC drift;
  drift.SetSensor(&sensor);
  drift.SetDistanceSteps(1.e-3);

  ViewDrift driftView;
  constexpr bool plotDrift = true;
  if (plotDrift) {
    aval .EnablePlotting(&driftView);
    drift.EnablePlotting(&driftView);
  }

  constexpr unsigned int nEvents = 1;
  for (unsigned int i = 1; i <= nEvents; ++i) {
    std::cout << i << "/" << nEvents << "\n";

    // Randomize the initial position.
    const double x0 = 0.25 * pitch; //-0.5 * pitch + RndmUniform() * pitch;
    const double y0 = 0.25 * pitch; //-0.5 * pitch + RndmUniform() * pitch;
    const double z0 = 0.101;
    const double t0 = 0.;
    const double e0 = 0.1;
//    aval.AvalancheElectron(x0, y0, z0, t0, e0, 0., 0., 0.);
    aval.AvalancheElectron(x0, y0, z0, t0, false);

//    int ne = 0, ni = 0;
    unsigned int ne = 0, ni = 0;
    aval.GetAvalancheSize(ne, ni);
    std::cout << ne << " " << ni << std::endl;

    // const unsigned int np = aval.GetNumberOfElectronEndpoints();
    // double xe1, ye1, ze1, te1, e1;
    // double xe2, ye2, ze2, te2, e2;
    // double xi1, yi1, zi1, ti1;
    // double xi2, yi2, zi2, ti2;
    // int status;
    // for (unsigned int j = 0; j < np; ++j) {
    //   aval.GetElectronEndpoint(j, xe1, ye1, ze1, te1, e1,
    //                               xe2, ye2, ze2, te2, e2, status);
    //   drift.DriftIon(xe1, ye1, ze1, te1);
    //   drift.GetIonEndpoint(0, xi1, yi1, zi1, ti1,
    //                           xi2, yi2, zi2, ti2, status);
    // }
  }

  if (plotDrift) {
    TCanvas* cd = new TCanvas();
    constexpr bool plotMesh = true;
    if (plotMesh) {
      ViewFEMesh* meshView = new ViewFEMesh();
      meshView->SetCanvas(cd);
      meshView->SetComponent(&fm);
      meshView->SetArea(-pitch, -pitch, -0.4,
                         pitch,  pitch,  0.4);
      // x-z projection.
      meshView->SetPlane(0, -1, 0, 0, 0, 0);
      meshView->SetFillMesh(true);

      // Set the color of each material.
      meshView->SetColor(0, kBlue);
      meshView->SetColor(1, kYellow +  3);
      meshView->SetColor(2, kOrange + 10);
      meshView->SetColor(3, kCyan   - 10);

      meshView->EnableAxes();
      meshView->SetViewDrift(&driftView);
      meshView->Plot(true);
    } else {
      driftView.SetPlane(0, -1, 0, 0, 0, 0);
      driftView.SetArea(-pitch, -0.4, pitch, 0.4);
      driftView.SetCanvas(cd);
      constexpr bool twod = true;
      driftView.Plot(twod);
    }
  }

  app.Run(kTRUE);
}
